# About
main is in MainCLI.java
This is a maven project
``` 
mvn package 
#(using java -version Openjdk 11) 
java -jar target/TicTacToe-0.9-SNAPSHOT.jar
```

# Example: 
```
*|*|*
-----
*|*|*
-----
*|*|*
Turn for: Player1
Playing as X
Please enter a number 0-8:
0

X|*|*
-----
*|*|*
-----
*|*|*
Turn for: Player2
Playing as O
Please enter a number 0-8:
1

X|O|*
-----
*|*|*
-----
*|*|*
Turn for: Player1
Playing as X
Please enter a number 0-8:
4

X|O|*
-----
*|X|*
-----
*|*|*
Turn for: Player2
Playing as O
Please enter a number 0-8:
2

X|O|O
-----
*|X|*
-----
*|*|*
Turn for: Player1
Playing as X
Please enter a number 0-8:
8

X|O|O
-----
*|X|*
-----
*|*|X
XWIN
```
# Tests
To run tests, please use ```mvn test```



# Todo 
- [X] Make into CLI

- [ ] Make into HTTP calls for a game (one or many games) for tic tac toe.
    - possibly use Jetty or Tomcat for POST/GETS
    - Stretch : Make a websocket interface for notifications. 