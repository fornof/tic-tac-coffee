/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.fornof.tictactoe;

import java.util.ArrayList;

/**
 *
 * @author Robert Fornof
 */
public class Board {

    private ArrayList<String> numBoard;

    public enum Result {
        XWIN,
        OWIN,
        TIE,
        ERROR,
        NONE,
        STILL_PLAYING,
    }
    private final ArrayList<String> grid;

    public Board() {
        this.grid = new ArrayList<>();
        this.initializeBoard();
        this.numBoard = null;
    }

    public ArrayList<String> getGrid() {
        return grid;
    }

    public ArrayList<String> setGrid() {
        return grid;
    }

    public void initializeBoard() { // BOARD fx
        for (int i = 0; i < 9; i++) {
            this.grid.add("");
        }
    }

    public Result strToResult(String result) {
        if (result.equals("X")) {
            return Result.XWIN;
        }
        if (result.equals("O")) {
            return Result.OWIN;
        }
        if (result.equals("&")) {
            return Result.TIE;
        }

        return Result.STILL_PLAYING;

    }

    public Result cases() {

        if (!(this.get(1, 1)).equals("")) {
            // diagonal right
            if (this.get(0, 0).equals(this.get(1, 1)) && this.get(1, 1).equals(this.get(2, 2))) {

                return this.strToResult(this.get(0, 0));
            }

            // diagonal left
            if (this.get(2, 0).equals(this.get(1, 1)) && this.get(1, 1).equals(this.get(0, 2))) {
                return this.strToResult(this.get(2, 0));
            }
        }
        // top-straight
        // mid -straight
        // bottom-straight
        for (int y = 0; y < 3; y++) {
            if (this.get(0, y).equals(this.get(1, y)) && this.get(1, y).equals(this.get(2, y)) && !this.get(0, y).equals("")) {
                return this.strToResult(this.get(0, y));
            }
        }
        // left-down
        // mid-down
        // right-down
        for (int x = 0; x < 3; x++) {
            if (this.get(x, 0).equals(this.get(x, 1)) && this.get(x, 1).equals(this.get(x, 2)) && !this.get(x, 0).equals("")) {
                return this.strToResult(this.get(x, 0));
            }
        }
        for (String str : this.grid) {
            if (str.equals("")) {
                return Result.STILL_PLAYING;
            }
        }
        // if it gets here, no empty squares are left, and no wins , so it is a tie
        return this.strToResult("&");
    }

    public Result validateBoard() {
        return cases();
    }

    public String get(int x, int y){
        String entry = this.grid.get(y * 3 + x);
        return entry;
    }

    public void place(String identifier, int x, int y) throws Exception {
        String entry = this.grid.get(y * 3 + x);
        if (!entry.equals("")) {
            throw new Exception("already placed, please try again");
        }
        this.grid.set(y * 3 + x, identifier);
    }

    public void place(String identifier, int idx) throws Exception {
        String entry = this.grid.get(idx);
        if (!entry.equals("")) {
            throw new Exception("already placed, please try again");
        }
        this.grid.set(idx, identifier);
    }

    public String printGridBoard() {
        if (this.numBoard == null) {
            System.out.println(this.numBoard);
            ArrayList<String> numGrid = new ArrayList<String>();
            for (int i = 0; i < 9; i++) {
                numGrid.add(i + "");
            }
            this.numBoard = numGrid;
        }
        return printBoardFromGrid(this.numBoard);

    }

    public String printBoard() {

        return printBoardFromGrid(this.grid);
    }

    public String printBoardFromGrid(ArrayList<String> grid) {

        StringBuilder sb = new StringBuilder();
        int size = grid.size();
        for (int i = 0; i < grid.size(); i++) {

            if (i % 3 == 0) {

                if (i != 0) {
                    sb.append("\n");
                    sb.append("-----\n");
                }
            } else {
                sb.append("|");
            }
            String entry = grid.get(i);
            if (entry.equals("")) {
                sb.append("*");
            } else {
                sb.append(entry);
            }
        }
        return sb.toString();
    }

}
