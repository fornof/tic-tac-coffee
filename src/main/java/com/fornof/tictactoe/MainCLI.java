/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.fornof.tictactoe;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainCLI {


    public MainCLI() {

    }
    public String getValidString() {
        
        String currentPlayerInput = null;
        boolean inputIsGood = false;
        while (!inputIsGood) {
            try {
                
                currentPlayerInput = readInput("Please enter a number 0-8: ");
                if(currentPlayerInput.equals("")){
                    throw new Exception("must be an entry");
                }
                inputIsGood = true;
            }catch(Exception e){
            //return
            System.out.println("that is not a number");
        }
            }
        return currentPlayerInput;
        }
    
    public Integer getValidNumberForGrid() {
        
        Integer currentPlayerInput = null;
        boolean inputIsGood = false;
        while (!inputIsGood) {
            try {
                
                currentPlayerInput = Integer.parseInt(readInput("Please enter a number 0-8: "));
                if(currentPlayerInput > 8 || currentPlayerInput < 0){
                    throw new Exception("must be between 0 and 8");
                }
                inputIsGood = true;
            }catch(Exception e){
            //return
            System.out.println("that is not a number");
        }
            }
        return currentPlayerInput;
        }
    

    public static void main(String args[]) {
        Boolean finished = false;
        Board board = new Board();
        MainCLI main = new MainCLI();
        int currentIdx = 0;
       // System.out.println("Player 1: Please enter your name");
       // Player player1 = new Player(getValidString(), "X");
       // System.out.println("Player 2: Please enter your name");
        //Player player2 = new Player(getValidString(), "O");
        String[] currentPlayer = new String[]{"Player1", "Player2"};
        String[] currentPlayerType = new String[]{"X", "O"};
        int currentidx = 0;
        String playerName = "";
        String playerType = "";
        Board.Result result = Board.Result.STILL_PLAYING;
        while (result.equals(Board.Result.STILL_PLAYING)) {
            
            playerName = currentPlayer[currentIdx];
            playerType = currentPlayerType[currentIdx];
            
            System.out.println(board.printBoard());
            System.out.println("Turn for: " + playerName + "\nPlaying as " + playerType);
            boolean finishedPlacement = false;
            while(!finishedPlacement){
                try {
                    board.place(playerType, main.getValidNumberForGrid());
                    finishedPlacement = true;
                } catch (Exception ex) {
                   System.out.println("That index has already been picked, please try again");
                }
            }
            result = board.validateBoard();
            currentIdx = (currentIdx + 1) % 2;
        }
        System.out.println(board.printBoard());
        System.out.println(result.name());
        // player chooses 1-9 , that corresponds to grid, offset by 1  
        // grid updates
        // player updates
    }

    public String readInput(String prompt) { // BOARD
        Scanner myObj = new Scanner(System.in);  // Create a Scanner object
        System.out.println(prompt);
        return myObj.nextLine();  // Read user input, all user input 

    }

}
