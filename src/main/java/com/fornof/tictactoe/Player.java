/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.fornof.tictactoe;

/**
 *
 * @author rob
 */
public class Player {

    private final String name;
    private final String identifier;
    public Player(String name, String identifier){
        this.name = name;
        this.identifier = identifier;
    }
}
