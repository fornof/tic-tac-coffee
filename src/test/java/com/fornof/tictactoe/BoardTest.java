/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.fornof.tictactoe;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author rob
 */
public class BoardTest {

    public BoardTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
        Board board = new Board();
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }

//    @BeforeAll
//    public static void setUpClass() {
//    }
//    
//    @AfterAll
//    public static void tearDownClass() {
//    }
//    
//    @BeforeEach
//    public void setUp() {
//    }
//    
//    @AfterEach
//    public void tearDown() {
//    }
    @org.junit.jupiter.api.Test
    public void testPrintBoardWorks() {
        // TODO review the generated test code and remove the default call to fail.
        Board board = new Board();
        System.out.print(board.printBoard());
        Assertions.assertEquals(
                "*|*|*\n"
                + "-----\n"
                + "*|*|*\n"
                + "-----\n"
                + "*|*|*", board.printBoard());
    }

    @org.junit.jupiter.api.Test
    public void testPrintFullBoardXs() {
        // TODO review the generated test code and remove the default call to fail.
        Board board = new Board();

        int x = 0;
        int y = 0;
        board.place("X", x++, y);
        board.place("X", x++, y);
        board.place("X", x++, y);
        x = 0;
        y = 1;
        board.place("X", x++, y);
        board.place("X", x++, y);
        board.place("X", x++, y);

        x = 0;
        y = 2;
        board.place("X", x++, y);
        board.place("X", x++, y);
        board.place("X", x++, y);
        System.out.print(board.printBoard());
        Assertions.assertEquals(
                "X|X|X\n"
                + "-----\n"
                + "X|X|X\n"
                + "-----\n"
                + "X|X|X", board.printBoard());
    }

    @org.junit.jupiter.api.Test
    public void validateWinCases() {
        // TODO review the generated test code and remove the default call to fail.
        Board board = new Board();
        board.place("X", 0, 0);
        board.place("X", 1, 1);
        board.place("X", 2, 2);
        var result = board.validateBoard(); // "XWIN", "oWIN", "TIE", "ERROR",
        Assertions.assertEquals(result, Board.Result.XWIN);

        board = new Board();
        board.place("O", 2, 0);
        board.place("O", 1, 1);
        board.place("O", 0, 2);
        result = board.validateBoard(); // "XWIN", "oWIN", "TIE", "ERROR",
        Assertions.assertEquals(result, Board.Result.OWIN);

        board = new Board();
        int x = 0;
        int y = 0;
        board.place("O", x++, y);
        board.place("X", x++, y);
        board.place("O", x++, y++);
        x = 0;
        board.place("X", x++, y);
        board.place("0", x++, y);
        board.place("X", x++, y++);
        x = 0;
        board.place("O", x++, y);
        board.place("X", x++, y);
        board.place("O", x++, y);

        result = board.validateBoard(); // "XWIN", "oWIN", "TIE", "ERROR",
        Assertions.assertEquals(result, Board.Result.TIE);

        board = new Board();
        x = 0;
        y = 0;
        board.place("O", x++, y);
        board.place("X", x++, y);
        board.place("O", x++, y++);
        x = 0;
        board.place("X", x++, y);
        board.place("0", x++, y);
        board.place("X", x++, y++);
        x = 0;
        board.place("O", x++, y);
        board.place("X", x++, y);
        result = board.validateBoard();
        Assertions.assertEquals(result, Board.Result.STILL_PLAYING);
        System.out.print(board.printBoard());
    }
}
